import React, {useEffect, useState } from 'react';
import './todo.scss'
const Todo = () => {
    let [inputTask, setInputTask] = useState({name: "", isFinished:false})
    let [todoList, setTodo] = useState(localStorage.getItem("todo") ? JSON.parse(localStorage.getItem("todo")):[]);
    console.log(localStorage.getItem("todo"));

    let [editIndex, setEdit] = useState({
        index: -1,
        name: "",
        isFinished: false
    })

    useEffect(() => {
        localStorage.setItem('todo', JSON.stringify(todoList));
      });



    const getEdit = (e) => {
        setEdit({ ...editIndex, name: e.target.value })
    }

    const getTodo = (e) => {
        setInputTask({ ...inputTask, name: e.target.value, isFinished: false })
    }

    const addTodo = () => {
       setTodo([...todoList, inputTask])
       localStorage.setItem("todo", JSON.stringify(todoList))
       
    }

    const updateTodo = (index) => {
        setEdit({ ...editIndex, index: index })
        if (editIndex.name.length > 0) {
            todoList[index].name = editIndex.name
            setEdit({ ...editIndex, name: "", index: -1 })
            localStorage.setItem('todo', JSON.stringify(todoList));
        }

    }

    const finishTodo = (index) => {
        todoList[index].isFinished = !todoList[index].isFinished
        setTodo([...todoList])
        localStorage.setItem('todo', JSON.stringify(todoList));
    }

    const deleteTodo = (index) => {
        todoList.splice(index, 1)
        setTodo([...todoList])
        localStorage.setItem('todo', JSON.stringify(todoList));

    }

    const handleSubmit = (e) => {
        e.preventDefault()
        e.target.reset();
    }

    let todos = null
    if (todoList.length >= 1) {
        todos = (

            <ul className="task-list">
                {todoList.map((todo, index) => {
                    return <li className="task-list-item">
                        <label className="task-list-item-label">
                            <input className="tick-todo" onClick={() => finishTodo(index)} type="checkbox"></input>
                            {editIndex.index === index ? <input type="text" onChange={getEdit} ></input> : <span>{todo.name}</span>}
                        </label>
                        <span className="delete-btn" title="Delete Task" onClick={() => deleteTodo(index)}></span>

                        <span onClick={() => updateTodo(index)} className="update-btn" ><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#98ff98" stroke-width="2"><path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path><polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon></svg></span>

                    </li>
                })}

            </ul>
        )



    }
    return (
        <form className="body" onSubmit={handleSubmit}>
            <div className="app-container" id="taskList">
                <h1 className="app-header">TO DO LIST</h1>
                <div className="add-task">
                    <input type="text" placeholder="Add New Task" className="task-input" onChange={getTodo}></input>
                    <input type="submit" value="" className="submit-task" title="Add Task" onClick={addTodo}></input>
                </div>

                {todos}
               
            </div>
        </form>
    )


}
export default Todo